import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

import {LoggerFactory} from '../helper/Logger';
import {AnyOrUndefined, Params, UndefinedOr} from '../types';

export abstract class AHttpService {
  protected abstract apiUrl: UndefinedOr<string>;
  protected version: string = '';
  protected readonly headers = new HttpHeaders().set('Content-Type', 'application/json');
  protected lumber = LoggerFactory.getLogger('AHttpService');

  protected constructor(protected http: HttpClient) {
  }

  public get(url: string, params?: Params, caller?: string): Observable<unknown> {
    return this.http.get(this.apiUrl + this.version + url, {params: params}).pipe(
      tap(data => {
        this.log('get', url, caller, data);
      }),
      catchError((error: any) => {
        this.log('get', url, caller, undefined, true);
        return this.error(error);
      })
    );
  }

  public post(url: string, body: any, params?: Params, caller?: string): Observable<unknown> {
    return this.http.post(this.apiUrl + this.version + url, body, {headers: this.headers, params: params}).pipe(
      tap(data => {
        this.log('post', url, caller, data);
      }),
      catchError((error: HttpErrorResponse) => {
        this.log('post', url, caller, undefined, true);
        return this.error(error);
      })
    );
  }

  public put(url: string, body: any, params?: Params, caller?: string): Observable<unknown> {
    return this.http.put(this.apiUrl + this.version + url, body, {headers: this.headers, params: params}).pipe(
      tap(data => {
        this.log('put', url, caller, data);
      }),
      catchError((error: HttpErrorResponse) => {
        this.log('put', url, caller, undefined, true);
        return this.error(error);
      })
    );
  }

  public delete(url: string, params?: Params, caller?: string): Observable<unknown> {
    return this.http.delete(this.apiUrl + this.version + url, {params: params}).pipe(
      tap(data => {
        this.log('delete', url, caller, data);
      }),
      catchError((error: HttpErrorResponse) => {
        this.log('delete', url, caller, undefined, true);
        return this.error(error);
      })
    );
  }

  protected _clientSideError(): void {
    // No implementation needed
    this.lumber.info('_clientSideError', 'Not implemented, but no implementation needed!');
  }

  protected _serverSideError(): void {
    // No implementation needed
    this.lumber.info('_clientSideError', 'Not implemented, but no implementation needed!');
  }

  protected error(error: any): any {
    if (error.error instanceof ErrorEvent) {
      // Client Side Error
      this._clientSideError();
      return throwError(error.error.message);
    } else if (error.status > 499){
      // Server Side Error
      this._serverSideError();
      return throwError(error.error.message);
    }
  }

  protected log(type: string, url: string, caller: UndefinedOr<string>, data: AnyOrUndefined, error: boolean = false): void {
    let string = 'URL: "' + url + '"';
    if (caller) {
      string += ' | caller: "' + caller + '"';
    }
    if (error) {
      this.lumber.error(type, string);
    } else {
      this.lumber.log(type, string, data);
    }
  }
}
